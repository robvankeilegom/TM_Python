import asyncio
import discord
from random import shuffle
import youtube_dl
import json
import os
import time
from threading import Thread
#import mad

if not discord.opus.is_loaded():
    # the 'opus' library here is opus.dll on windows
    # or libopus.so on linux in the current directory
    # you should replace this with the location the
    # opus library is located in and with the proper filename.
    discord.opus.load_opus('opus')

class MyLogger(object):
    def debug(self, msg):
        #print(msg)
        pass 
    
    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


        
class Bot(discord.Client):
    def __init__(self):
        super().__init__()
        self.playlist = list()
        self.volgende = None
        self.playing = None
        self.nu = None
        self.ydl_opts = {}
        self.nu_file = None
        self.volgende_file = None
        #self.nu_time = None
        #self.volgende_time = None
        self.file = None
        self.player = None
        #self.test = 0
        self.game = None
        self.loop = asyncio.get_event_loop()
        self.lastmessage = None
        self.defaultchannel = None
        self.server = None
        
    def my_hook(self, d):
        if d['status'] == 'finished':
            #print('Done downloading, now converting ...')
            self.file = d['filename']
            
    def download(self, link):
        ydl_opts = {
                    'format': 'bestaudio/best',
                    'postprocessors': [{
                                        'key': 'FFmpegExtractAudio',
                                        'preferredcodec': 'mp3',
                                        'preferredquality': '192',
                                        }], 
                    'logger': MyLogger(),
                    'progress_hooks': [self.my_hook], 'nooverwrites': True
                    }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([link])
        head, sep, tail = self.file.rpartition('.')
        self.file = head + sep + 'mp3'
        print(self.file)
        return    
        
    def initsong(self):
        if self.nu == None and self.playlist:
            self.nu = self.playlist.pop(0)
            self.download(self.nu)
            self.nu_file = self.file
            #mf = mad.MadFile(self.nu_file)
            #self.nu_time = (mf.total_time()) * 1000 #omzetten in sec
        if self.volgende == None and self.playlist:
            self.volgende = self.playlist.pop(0)
            self.download(self.volgende)
            self.volgende_file = self.file
            #mf = mad.MadFile(self.volgende_file)
            #self.volgende_time = (mf.total_time()) * 1000 #omzetten in sec 
        return
    
    def check_playing(self):
        while True:
            if hasattr(self.player, 'is_playing'):
                if self.player.is_playing() == False:
                    #print(self.player.is_playing())
                    self.next_song()
            else:
                #print('break')
                break
            time.sleep(5)
            #print('running')
            
    def update_playing(self):
        if self.nu_file != None:
            naam, sep2, tail2 = self.nu_file.rpartition('-')
            tmpgame = discord.Game(name = naam)
            if self.game != tmpgame:
                self.game = tmpgame
                self.nowplaying()
                #TODO send message
        else:
            self.game = None
            
        self.loop.create_task(self.change_status(self.game))
        return
        
    def next_song(self):
        #print(self.player)
        if self.player != None:
            #print("if 1")
            if hasattr(self.player, 'is_playing'):
                self.player.stop()
                self.player = None
        if self.volgende_file != self.nu_file:
            #print("if 2")
            self.nu = self.volgende
            os.remove(self.nu_file)
            self.nu_file = self.volgende_file
            #self.nu_time = self.volgende_time
            self.volgende = None
            self.volgende_file = None
            #self.volgende_time = None
        else:
            #print("else 2")
            self.volgende = None
            self.volgende_file = None
        if self.nu != None:
            self.player = self.voice_client_in(self.server).create_ffmpeg_player(self.nu_file)
            self.player.start()
            self.initsong()
        self.update_playing()
        return
            
    def printkappa(self):
        self.test += 1
        print(self.test)
        print("playlist".center(20,'-'))
        print("\n".join(self.playlist))
        print("self.nu")
        print(self.nu)
        print("self.volgende")
        print(self.volgende )
        print("files".center(20,'-'))
        print("nu")
        print(self.nu_file)
        print("volgende")
        print(self.volgende_file)

    def manage_new_message(self, text):
        if self.lastmessage !=None:
            self.loop.create_task(self.delete_message(self.lastmessage))
            self.lastmessage = None
            
        self.loop.create_task(self.send_message(self.defaultchannel, text))

    def nowplaying(self):
        if self.nu_file == None:
            naam = "None"
        else:
            naam, sep2, tail2 = self.nu_file.rpartition('-')
        if self.volgende_file == None:
            naam2 = "None"
        else:
            naam2, sep2, tail2 = self.volgende_file.rpartition('-')
        self.manage_new_message('```\nNow playing: \n' + naam + '\nNext song: \n' + naam2 + '\n```')
                    
    async def on_message(self, message):
        self.defaultchannel = discord.utils.get(message.server.channels, name='botchannel', type=discord.ChannelType.text)
        if self.defaultchannel == None:
            self.defaultchannel = message.channel
        if message.author == self.user:
            self.lastmessage = message
            return
        elif message.content.startswith('!play'):
            #if self.player is not None and self.player.is_playing():
            #    await self.send_message(message.channel, 'Already playing a song')
            #   return
            if not self.is_voice_connected(self.server):
                self.manage_new_message('Not connected to a voice channel. Use !joinchannel')
                #self.lastmessage = await self.send_message(message.channel, 'Not connected to a voice channel. Use !joinchannel')
                return
            link = message.content[5:].strip()
            if link == "":
                self.manage_new_message("Er is geen link mee gegeven")
            else:
                print(link)
                self.add_links(link)
                if self.volgende == None:
                    self.initsong()
            if self.player is None:
                #print('aanmaak')
                self.player = self.voice_client_in(self.server).create_ffmpeg_player(self.nu_file)
                #print('start')
            #if self.player.is_done():
                self.player.start()
            if __name__ == "__main__": #checks if the module isn't imported
                t1 = Thread(target = self.check_playing)
                t1.setDaemon(True)
                t1.start()
            if self.game == None:
                self.update_playing()
            
            
        elif message.content.startswith('!print'):
            self.printkappa()
        
        elif message.content.startswith('!nowplaying'):
            self.nowplaying()
        #elif message.content.startswith('!delete'):
            #if self.lastmessage != None:
                #await self.delete_message(self.lastmessage)
                #self.lastmessage = None
            
        elif message.content.startswith('!clear'):
            self.playlist = list()
            
        elif message.content.startswith('!shuffle'):
            shuffle(self.playlist)
            
        elif message.content.startswith("!joinchannel"):
            if self.is_voice_connected(message.server):
                return
            memberid = message.author.id
            server = message.server
            for channel in server.channels:
                if discord.utils.get(channel.voice_members, id=memberid):
                    print(channel)
                    await self.join_voice_channel(channel)
                    self.server = server
                    break
                    
        elif message.content.startswith("!next"):
            #print(self.player)
            self.player.stop()
            self.player = None
            self.next_song()
            
        elif message.content.startswith("!dc"):
            #if self.is_voice_connected():
             
            await self.voice_client_in(message.server).disconnect()
                
    def add_links(self, link):
        ydl_opts = {'logger': MyLogger(),'ignoreerrors':True, "default_search" : 'auto'}
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            info_dict = ydl.extract_info(link, download=False)
            strinput = json.dumps(info_dict)
            jsondata = json.loads(strinput)
            if 'entries' in jsondata:
                for row in jsondata['entries']:
                    if row != None:
                        self.playlist.append(row['webpage_url'])
            else:
                self.playlist.append(jsondata['webpage_url'])
        self.manage_new_message('Links added to playlist')
        return
    
        
    def stop(self):
        self.playing = False
        
    
    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')
        self.game = None
        self.loop.create_task(self.change_status(self.game))




bot = Bot()
bot.run('')
